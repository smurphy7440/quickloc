import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import Vue2LeafletRotatedMarker from 'vue2-leaflet-rotatedmarker';
import { Icon } from 'leaflet';
import VueRouter from 'vue-router';
import MapLoader from './components/MapLoader.vue';
import Messages from './components/Messages.vue';

Vue.use(VueRouter);

delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
  iconRetinaUrl: require('./assets/images/ic_new_white_taxi.png'),
  iconUrl: require('./assets/images/ic_new_white_taxi.png'),
  iconSize:[50, 50],
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

const routes = [
  {path:'/', component: MapLoader},
  {path:'/messages', component:Messages}
]

const router = new VueRouter({
  routes
})

Vue.config.productionTip = false;

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.component('l-rotated-marker', Vue2LeafletRotatedMarker);

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
